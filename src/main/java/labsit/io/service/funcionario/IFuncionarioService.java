package labsit.io.service.funcionario;

import labsit.io.controller.funcionario.FuncionarioDto;
import labsit.io.service.IService;

public interface IFuncionarioService extends IService<FuncionarioDto, String> {
}
