package labsit.io.service.funcionario;


import labsit.io.controller.funcionario.FuncionarioDto;
import labsit.io.entity.funcionario.Funcionario;
import labsit.io.entity.funcionario.FuncionarioBuilder;
import labsit.io.repository.IFuncionarioRepository;
import labsit.io.repository.mapper.FuncionarioMapper;
import labsit.io.service.ServiceException;
import labsit.io.utils.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class FuncionarioService implements IFuncionarioService {
  private final IFuncionarioRepository repository;

  public FuncionarioService(IFuncionarioRepository funcionarioRepository) {
    this.repository = funcionarioRepository;
  }

  public void salvar(FuncionarioDto funcionarioDto) {
    Funcionario funcionario = getFuncionario(funcionarioDto);
    funcionario.validarDados();
    if (this.repository.findByCpf(funcionario.getCpf()).isPresent()) {
      throw new ServiceException("cpf já está sendo utilizado");
    }
    if (this.repository.findByApelido(funcionario.getApelido()).isPresent()) {
      throw new ServiceException("apelido já está sendo utilizado");
    }
    this.repository.insert(FuncionarioMapper.toMapper(funcionario));
  }

  public void atualizar(String id, FuncionarioDto funcionarioDto) {
    Funcionario funcionario = getFuncionario(funcionarioDto);
    if (Objects.isNull(id) || id.isBlank()) {
      throw new ServiceException("identificador do funcionário não foi informado");
    }
    funcionario.validarDados();
    this.repository.findById(id).ifPresent(mapper -> {
      this.repository.save(FuncionarioMapper.toMapper(id, funcionario, mapper));
    });

  }

  @Override
  public void deletar(String id) {
    Optional<FuncionarioMapper> funcionario = this.repository.findById(id);
    funcionario.ifPresent(this.repository::delete);
  }

  private static Funcionario getFuncionario(FuncionarioDto funcionarioDto) {
    return FuncionarioBuilder.builder()
        .apelido(funcionarioDto.getApelido())
        .cpf(funcionarioDto.getCpf())
        .nome(funcionarioDto.getNome())
        .sobrenome(funcionarioDto.getSobrenome())
        .cargo(funcionarioDto.getCargo())
        .departamento(funcionarioDto.getDepartamento())
        .senha(funcionarioDto.getSenha())
        .apelido(funcionarioDto.getApelido())
        .validator(Validator.get())
        .build();
  }

}
