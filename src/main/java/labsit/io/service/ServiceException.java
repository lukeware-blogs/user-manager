package labsit.io.service;

public class ServiceException extends RuntimeException {

  public ServiceException(String mensagem) {
    super(mensagem);
  }
}
