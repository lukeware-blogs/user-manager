package labsit.io.service.cliente;

import labsit.io.controller.cliente.ClienteDto;
import labsit.io.service.IService;

public interface IClienteService extends IService<ClienteDto, String> {
}
