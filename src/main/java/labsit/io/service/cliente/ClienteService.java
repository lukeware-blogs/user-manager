package labsit.io.service.cliente;


import labsit.io.controller.cliente.ClienteDto;
import labsit.io.entity.cliente.Cliente;
import labsit.io.entity.cliente.ClienteBuilder;
import labsit.io.repository.IClienteRepository;
import labsit.io.repository.mapper.ClienteMapper;
import labsit.io.service.ServiceException;
import labsit.io.utils.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class ClienteService implements IClienteService {

  private final IClienteRepository repository;

  public ClienteService(IClienteRepository clienteRepository) {
    this.repository = clienteRepository;
  }

  public void salvar(ClienteDto clientedto) {
    Cliente cliente = getCliente(clientedto);
    cliente.validarDados();
    if (this.repository.findByCpf(cliente.getCpf()).isPresent()) {
      throw new ServiceException("cpf já está sendo utilizado");
    }
    if (this.repository.findByEmail(cliente.getEmail()).isPresent()) {
      throw new ServiceException("email já está sendo utilizado");
    }
    this.repository.save(ClienteMapper.toMapper(cliente));
  }

  public void atualizar(String id, ClienteDto clienteDto) {
    Cliente cliente = getCliente(clienteDto);
    cliente.validarDados();
    if (Objects.isNull(id) || id.isBlank()) {
      throw new ServiceException("identificador do cliente foi informado");
    }
    this.repository.findById(id).ifPresent(mapper -> {
      this.repository.save(ClienteMapper.toMapper(id, cliente, mapper));
    });
  }

  @Override
  public void deletar(String id) {
    Optional<ClienteMapper> funcionario = this.repository.findById(id);
    funcionario.ifPresent(this.repository::delete);
  }

  private static Cliente getCliente(ClienteDto clientedto) {
    return ClienteBuilder.builder()
        .cpf(clientedto.getCpf())
        .nome(clientedto.getNome())
        .senha(clientedto.getSenha())
        .email(clientedto.getEmail())
        .sobrenome(clientedto.getSobrenome())
        .endereco(clientedto.getEndereco())
        .telefone(clientedto.getTelefone())
        .validator(Validator.get())
        .build();
  }

}
