package labsit.io.service;

public interface IService<T, ID> {
  void salvar(T t);

  void atualizar(ID id, T t);

  void deletar(ID id);
}
