package labsit.io.controller.cliente;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public final class ClienteDto {
  private String id;
  private String telefone;
  private String endereco;
  private String email;
  private String senha;
  private String nome;
  private String sobrenome;
  private String cpf;
}
