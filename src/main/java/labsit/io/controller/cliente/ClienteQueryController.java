package labsit.io.controller.cliente;

import labsit.io.controller.AbstractQueryController;
import labsit.io.repository.IClienteRepository;
import labsit.io.repository.mapper.ClienteMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/clientes")
class ClienteQueryController extends AbstractQueryController<IClienteRepository, ClienteMapper> {
  ClienteQueryController(IClienteRepository clienteRepository) {
    super(clienteRepository);
  }

  @GetMapping("/search")
  Page<ClienteMapper> pesquisar(
      @RequestParam String nome,
      @RequestParam String endereco,
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "10") int size) {
    return repository.findByNomeIgnoreCaseContainingAndEnderecoIgnoreCaseContaining(nome, endereco, PageRequest.of(page, size, Sort.Direction.DESC, "data_criacao"));
  }
}
