package labsit.io.controller.cliente;

import labsit.io.controller.AbstractCommandController;
import labsit.io.service.cliente.IClienteService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/clientes")
class ClienteCommandController extends AbstractCommandController<IClienteService, ClienteDto> {
  ClienteCommandController(IClienteService clienteService) {
    super(clienteService);
  }
}
