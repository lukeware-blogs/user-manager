package labsit.io.controller;

import labsit.io.entity.cliente.ClienteException;
import labsit.io.entity.pessoa.PessoaException;
import labsit.io.entity.usuario.UsuarioException;
import labsit.io.service.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

@RestControllerAdvice
class ControollerAdvice {

  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler({ServiceException.class, ClienteException.class, UsuarioException.class, PessoaException.class})
  Map<String, Object> execeptionHandler(RuntimeException exception, HttpServletRequest req) {
    Map<String, Object> error = new LinkedHashMap<>();
    error.put("timestamp", LocalDateTime.now());
    error.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
    error.put("error", exception.getMessage());
    StackTraceElement stackTraceElement = Arrays.stream(exception.getStackTrace()).findFirst().get();
    error.put("detail", stackTraceElement.getClassName() + " - line(" + stackTraceElement.getLineNumber() + ")");
    error.put("path", req.getRequestURI());
    return error;
  }


}
