package labsit.io.controller.funcionario;

import labsit.io.controller.AbstractQueryController;
import labsit.io.repository.IFuncionarioRepository;
import labsit.io.repository.mapper.FuncionarioMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/funcionarios")
class FuncionarioQueryController extends AbstractQueryController<IFuncionarioRepository, FuncionarioMapper> {
  FuncionarioQueryController(IFuncionarioRepository funcionarioRepository) {
    super(funcionarioRepository);
  }

  @GetMapping("/search")
  Page<FuncionarioMapper> pesquisar(
      @RequestParam String cargo,
      @RequestParam String departamento,
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "10") int size) {
    return repository.findByCargoIgnoreCaseContainingAndDepartamentoIgnoreCaseContaining(cargo,
        departamento,
        PageRequest.of(page, size, Sort.Direction.DESC, "data_criacao")
    );
  }

}
