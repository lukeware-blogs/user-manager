package labsit.io.controller.funcionario;

import labsit.io.controller.AbstractCommandController;
import labsit.io.service.funcionario.IFuncionarioService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/funcionarios")
class FuncionarioCommandController extends AbstractCommandController<IFuncionarioService, FuncionarioDto> {
  FuncionarioCommandController(IFuncionarioService funcionarioService) {
    super(funcionarioService);
  }
}
