package labsit.io.controller.funcionario;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FuncionarioDto {
  private String apelido;
  private String senha;
  private String nome;
  private String sobrenome;
  private String cpf;
  private String cargo;
  private String departamento;
}
