package labsit.io.controller;


import labsit.io.service.IService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public abstract class AbstractCommandController<A extends IService<C, String>, C> {

  private final A service;

  protected AbstractCommandController(A service) {
    this.service = service;
  }

  @PostMapping
  protected void salvar(@RequestBody C object) {
    this.service.salvar(object);
  }

  @PutMapping("/{id}")
  protected void alterar(@PathVariable String id, @RequestBody C cliente) {
    this.service.atualizar(id, cliente);
  }

  @DeleteMapping("/{id}")
  protected void deletar(@PathVariable String id) {
    this.service.deletar(id);
  }

}
