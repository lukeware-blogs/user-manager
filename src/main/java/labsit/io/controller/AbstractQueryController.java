package labsit.io.controller;

import labsit.io.repository.IPessoaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

public abstract class AbstractQueryController<A extends IPessoaRepository<C, String>, C> {

  protected final A repository;

  protected AbstractQueryController(A repository) {
    this.repository = repository;
  }

  @GetMapping("/{id}")
  Optional<C> buscarPorId(@PathVariable String id) {
    return repository.findById(id);
  }

  @GetMapping()
  Page<C> buscarTodosPaginado(@RequestParam int page, @RequestParam int size) {
    return repository.findAll(PageRequest.of(page, size));
  }

  @GetMapping("/last")
  Page<C> buscarOsUltimos(@RequestParam(defaultValue = "10") int limit) {
    return repository.findAll(PageRequest.of(0, limit, Sort.Direction.DESC, "data_criacao"));
  }

  @GetMapping("/cpf")
  Optional<C> buscarPorCpf(@RequestParam String cpf) {
    return repository.findByCpf(cpf);
  }

}
