package labsit.io.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface IPessoaRepository<T, ID> extends MongoRepository<T, ID> {
  Optional<T> findByCpf(String cpf);
}
