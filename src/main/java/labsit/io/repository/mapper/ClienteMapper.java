package labsit.io.repository.mapper;

import labsit.io.entity.cliente.ICliente;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Document(value = "customer")
public final class ClienteMapper extends PessoaMapper {

  private String telefone;
  private String endereco;
  private String email;

  public static ClienteMapper toMapper(String id, ICliente cliente, ClienteMapper mapper) {
    ClienteMapper clienteMapper = toMapper(cliente);
    clienteMapper.dataAtualizacao = LocalDateTime.now();
    clienteMapper.dataCriacao = mapper.dataCriacao;
    clienteMapper.id = id;
    return clienteMapper;
  }

  public static ClienteMapper toMapper(ICliente cliente) {
    ClienteMapper clienteMapper = new ClienteMapper();
    clienteMapper.email = cliente.getEmail();
    clienteMapper.senha = cliente.getSenha();
    clienteMapper.nome = cliente.getNome();
    clienteMapper.sobrenome = cliente.getSobrenome();
    clienteMapper.cpf = cliente.getCpf();
    clienteMapper.endereco = cliente.getEndereco();
    clienteMapper.telefone = cliente.getTelefone();
    clienteMapper.dataCriacao = LocalDateTime.now();
    return clienteMapper;
  }
}
