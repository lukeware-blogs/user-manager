package labsit.io.repository.mapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public abstract class PessoaMapper {
  @Id
  protected String id;
  protected String senha;
  protected String nome;
  protected String sobrenome;
  protected String cpf;
  @Field("data_atualizacao")
  @JsonProperty("data_atualizacao")
  protected LocalDateTime dataAtualizacao;
  @Field("data_criacao")
  @JsonProperty("data_criacao")
  protected LocalDateTime dataCriacao;
}
