package labsit.io.repository.mapper;

import labsit.io.entity.funcionario.Funcionario;
import labsit.io.entity.funcionario.IFuncionario;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Document(value = "employee")
public final class FuncionarioMapper extends PessoaMapper {

  private String apelido;
  private String cargo;
  private String departamento;


  public static FuncionarioMapper toMapper(IFuncionario funcionario) {
    FuncionarioMapper funcionarioMapper = new FuncionarioMapper();
    funcionarioMapper.apelido = funcionario.getApelido();
    funcionarioMapper.senha = funcionario.getSenha();
    funcionarioMapper.nome = funcionario.getNome();
    funcionarioMapper.sobrenome = funcionario.getSobrenome();
    funcionarioMapper.cpf = funcionario.getCpf();
    funcionarioMapper.cargo = funcionario.getCargo();
    funcionarioMapper.departamento = funcionario.getDepartamento();
    funcionarioMapper.dataCriacao = LocalDateTime.now();
    return funcionarioMapper;
  }

  public static FuncionarioMapper toMapper(String id, Funcionario funcionario, FuncionarioMapper mapper) {
    FuncionarioMapper funcionarioMapper = toMapper(funcionario);
    funcionarioMapper.dataCriacao = mapper.dataCriacao;
    funcionarioMapper.id = id;
    funcionarioMapper.dataAtualizacao = LocalDateTime.now();
    return funcionarioMapper;
  }
}
