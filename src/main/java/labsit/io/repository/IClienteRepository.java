package labsit.io.repository;

import labsit.io.repository.mapper.ClienteMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface IClienteRepository extends IPessoaRepository<ClienteMapper, String> {
  Page<ClienteMapper> findByNomeIgnoreCaseContainingAndEnderecoIgnoreCaseContaining(String nome, String endereco, Pageable pageable);
  Optional<ClienteMapper> findByEmail(String email);
}
