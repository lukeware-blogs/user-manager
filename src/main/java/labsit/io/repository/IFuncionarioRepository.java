package labsit.io.repository;

import labsit.io.repository.mapper.FuncionarioMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface IFuncionarioRepository extends IPessoaRepository<FuncionarioMapper, String> {
  Page<FuncionarioMapper> findByCargoIgnoreCaseContainingAndDepartamentoIgnoreCaseContaining(String cargo, String departamento, Pageable pageable);
  Optional<FuncionarioMapper> findByApelido(String apelido);
}
