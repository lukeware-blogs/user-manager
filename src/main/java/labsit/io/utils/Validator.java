package labsit.io.utils;

import java.util.Objects;

public final class Validator implements IValidator {

  private static Validator validator;

  private Validator() {
    super();
  }

  public static Validator get() {
    if (Objects.isNull(validator)) {
      validator = new Validator();
    }
    return validator;
  }

  @Override
  public void isValid(String value, RuntimeException exception) {
    if (Objects.isNull(value)) {
      throw exception;
    }
    if (value.isBlank()) {
      throw exception;
    }
  }

}
