package labsit.io.utils;

public interface IValidator {
  void isValid(String value, RuntimeException exception);
}
