package labsit.io.entity.cliente;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public final class Cliente extends UsuarioCliente implements ICliente {
  private String telefone;
  private String endereco;

  @Override
  public void validarDados() {
    super.validarDados();
    this.validator.isValid(this.telefone, new ClienteException("telefone não foi informado"));
    this.validator.isValid(this.endereco, new ClienteException("endereço não foi informado"));
  }
}
