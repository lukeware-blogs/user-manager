package labsit.io.entity.cliente;

public interface ICliente {

  String getEmail();

  String getSenha();

  String getNome();

  String getSobrenome();

  String getCpf();

  String getEndereco();

  String getTelefone();
}
