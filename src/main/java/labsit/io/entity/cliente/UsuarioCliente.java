package labsit.io.entity.cliente;

import labsit.io.entity.usuario.Usuario;
import labsit.io.entity.usuario.UsuarioException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class UsuarioCliente extends Usuario {
  protected String email;


  @Override
  protected void validarDados() {
    super.validarDados();
    this.validator.isValid(this.email, new UsuarioException("email não foi informado"));
  }

}
