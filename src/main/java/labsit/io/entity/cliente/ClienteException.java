package labsit.io.entity.cliente;

import labsit.io.entity.usuario.UsuarioException;

public class ClienteException extends UsuarioException {
  public ClienteException(String mensagem) {
    super(mensagem);
  }
}
