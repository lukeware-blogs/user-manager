package labsit.io.entity.cliente;

import labsit.io.utils.IValidator;

import java.util.Objects;

public final class ClienteBuilder {

  private static ClienteBuilder builder;
  private String email;
  private String senha;
  private String nome;
  private String sobrenome;
  private String cpf;
  private String endereco;
  private String telefone;
  private IValidator validator;

  private ClienteBuilder() {
    super();
  }

  public static ClienteBuilder builder() {
    if (Objects.isNull(builder)) {
      builder = new ClienteBuilder();
    }
    return builder;
  }

  public ClienteBuilder email(String email) {
    this.email = email;
    return this;
  }

  public ClienteBuilder senha(String senha) {
    this.senha = senha;
    return this;
  }

  public ClienteBuilder nome(String nome) {
    this.nome = nome;
    return this;
  }

  public ClienteBuilder sobrenome(String sobrenome) {
    this.sobrenome = sobrenome;
    return this;
  }

  public ClienteBuilder cpf(String cpf) {
    this.cpf = cpf;
    return this;
  }

  public ClienteBuilder endereco(String endereco) {
    this.endereco = endereco;
    return this;
  }

  public ClienteBuilder telefone(String telefone) {
    this.telefone = telefone;
    return this;
  }

  public ClienteBuilder validator(IValidator validator) {
    this.validator = validator;
    return this;
  }

  public Cliente build() {
    Cliente cliente = new Cliente();
    cliente.setSenha(this.senha);
    cliente.setTelefone(this.telefone);
    cliente.setNome(this.nome);
    cliente.setEndereco(this.endereco);
    cliente.setEmail(this.email);
    cliente.setCpf(this.cpf);
    cliente.setSobrenome(this.sobrenome);
    cliente.setValidator(validator);
    return cliente;
  }

}
