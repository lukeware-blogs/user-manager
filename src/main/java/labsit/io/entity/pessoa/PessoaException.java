package labsit.io.entity.pessoa;

public class PessoaException extends RuntimeException {
  public PessoaException(String mensagem) {
    super(mensagem);
  }
}
