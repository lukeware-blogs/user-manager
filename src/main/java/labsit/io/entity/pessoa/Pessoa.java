package labsit.io.entity.pessoa;


import labsit.io.utils.IValidator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public abstract class Pessoa {
  protected String nome;
  protected String sobrenome;
  protected String cpf;
  protected IValidator validator;

  protected void validarDados() {
    this.validator.isValid(this.nome, new PessoaException("nome não foi informado"));
    this.validator.isValid(this.sobrenome, new PessoaException("sobrenome não foi informado"));
    this.validator.isValid(this.cpf, new PessoaException("cpf não foi informado"));
  }

}
