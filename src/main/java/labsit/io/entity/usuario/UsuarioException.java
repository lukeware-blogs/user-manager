package labsit.io.entity.usuario;

import labsit.io.entity.pessoa.PessoaException;

public class UsuarioException extends PessoaException {
  public UsuarioException(String mensagem) {
    super(mensagem);
  }
}
