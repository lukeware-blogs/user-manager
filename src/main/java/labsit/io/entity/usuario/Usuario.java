package labsit.io.entity.usuario;

import labsit.io.entity.pessoa.Pessoa;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class Usuario extends Pessoa {
  protected String senha;

  @Override
  protected void validarDados() {
    super.validarDados();
    this.validator.isValid(this.senha, new UsuarioException("senha não foi informada"));
  }

}
