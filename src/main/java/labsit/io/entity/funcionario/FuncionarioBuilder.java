package labsit.io.entity.funcionario;

import labsit.io.utils.IValidator;

import java.util.Objects;

public final class FuncionarioBuilder {

  private static FuncionarioBuilder builder;
  private String apelido;
  private String senha;
  private String nome;
  private String sobrenome;
  private String cpf;
  private String cargo;
  private String departamento;
  private IValidator validator;

  private FuncionarioBuilder() {
    super();
  }

  public static FuncionarioBuilder builder() {
    if (Objects.isNull(builder)) {
      builder = new FuncionarioBuilder();
    }
    return builder;
  }

  public FuncionarioBuilder apelido(String apelido) {
    this.apelido = apelido;
    return this;
  }

  public FuncionarioBuilder senha(String senha) {
    this.senha = senha;
    return this;
  }

  public FuncionarioBuilder nome(String nome) {
    this.nome = nome;
    return this;
  }

  public FuncionarioBuilder sobrenome(String sobrenome) {
    this.sobrenome = sobrenome;
    return this;
  }

  public FuncionarioBuilder cpf(String cpf) {
    this.cpf = cpf;
    return this;
  }

  public FuncionarioBuilder cargo(String cargo) {
    this.cargo = cargo;
    return this;
  }

  public FuncionarioBuilder departamento(String departamento) {
    this.departamento = departamento;
    return this;
  }

  public FuncionarioBuilder validator(IValidator validator) {
    this.validator = validator;
    return this;
  }

  public Funcionario build() {
    Funcionario funcionario = new Funcionario();
    funcionario.setSenha(this.senha);
    funcionario.setDepartamento(this.departamento);
    funcionario.setNome(this.nome);
    funcionario.setCargo(this.cargo);
    funcionario.setApelido(this.apelido);
    funcionario.setCpf(this.cpf);
    funcionario.setSobrenome(this.sobrenome);
    funcionario.setValidator(this.validator);
    return funcionario;
  }

}
