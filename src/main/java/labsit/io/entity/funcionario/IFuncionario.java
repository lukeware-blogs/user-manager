package labsit.io.entity.funcionario;

public interface IFuncionario {

  String getApelido();

  String getSenha();

  String getNome();

  String getSobrenome();

  String getCpf();

  String getCargo();

  String getDepartamento();
}
