package labsit.io.entity.funcionario;

import labsit.io.entity.usuario.UsuarioException;

public class FuncionarioException extends UsuarioException {
  public FuncionarioException(String mensagem) {
    super(mensagem);
  }
}
