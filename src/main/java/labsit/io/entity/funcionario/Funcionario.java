package labsit.io.entity.funcionario;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public final class Funcionario extends UsuarioFuncionario implements IFuncionario {
  private String cargo;
  private String departamento;

  @Override
  public void validarDados() throws FuncionarioException {
    super.validarDados();
    this.validator.isValid(this.departamento, new FuncionarioException("departamento não foi informado"));
    this.validator.isValid(this.cargo, new FuncionarioException("cargo não foi informado"));
  }
}
