package labsit.io.entity.funcionario;

import labsit.io.entity.usuario.Usuario;
import labsit.io.entity.usuario.UsuarioException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public abstract class UsuarioFuncionario extends Usuario {
  protected String apelido;

  @Override
  protected void validarDados() {
    super.validarDados();
    this.validator.isValid(this.apelido, new UsuarioException("apelido não foi informada"));
  }
}
