package labsit.io.repository;

import labsit.io.repository.mapper.FuncionarioMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Collectors;

@DataMongoTest
@ActiveProfiles("test")
class IFuncionarioRepositoryTest {

  @Autowired
  IFuncionarioRepository funcionarioRepository;

  @Autowired
  MongoTemplate mongoTemplate;

  @BeforeEach
  void setUp() {
    FuncionarioMapper funcionarioMapper = new FuncionarioMapper();
    funcionarioMapper.setSobrenome("Morais");
    funcionarioMapper.setDataCriacao(LocalDateTime.now());
    funcionarioMapper.setNome("Diego");
    funcionarioMapper.setCpf("999.999.999-99");
    funcionarioMapper.setApelido("diego.morais");
    funcionarioMapper.setCargo("desenvolvedor de software");
    funcionarioMapper.setDepartamento("Engine Software");
    mongoTemplate.insert(funcionarioMapper);
  }

  @AfterEach
  void tearDown() {
    mongoTemplate.dropCollection(FuncionarioMapper.class);
  }


  @Test
  void devera_buscar_por_cargo_departamento() {
    PageRequest of = PageRequest.of(0, 1);
    Page<FuncionarioMapper> funcionario = funcionarioRepository.findByCargoIgnoreCaseContainingAndDepartamentoIgnoreCaseContaining("desenvolvedor", "Engine", of);


    Assertions.assertThat(funcionario).isNotNull();
    Assertions.assertThat(funcionario.get().collect(Collectors.toList())).isNotEmpty();
    Assertions.assertThat(funcionario.getTotalElements()).isEqualTo(1);
    Assertions.assertThat(funcionario.get().findFirst().isPresent()).isTrue();
    Assertions.assertThat(funcionario.get().findFirst().isEmpty()).isFalse();
    Assertions.assertThat(funcionario.get().findFirst().get().getCpf()).isEqualTo("999.999.999-99");
    Assertions.assertThat(funcionario.get().findFirst().get().getDepartamento()).isEqualTo("Engine Software");
    Assertions.assertThat(funcionario.get().findFirst().get().getCargo()).isEqualTo("desenvolvedor de software");

  }

  @Test
  void devera_buscar_pelo_apelido() {
    PageRequest of = PageRequest.of(0, 1);
    Optional<FuncionarioMapper> funcionario = funcionarioRepository.findByApelido("diego.morais");


    Assertions.assertThat(funcionario).isNotNull();
    Assertions.assertThat(funcionario.isEmpty()).isFalse();
    Assertions.assertThat(funcionario.isPresent()).isTrue();
    Assertions.assertThat(funcionario.get().getCpf()).isEqualTo("999.999.999-99");
    Assertions.assertThat(funcionario.get().getDepartamento()).isEqualTo("Engine Software");
    Assertions.assertThat(funcionario.get().getCargo()).isEqualTo("desenvolvedor de software");
  }
}