package labsit.io.repository;

import labsit.io.repository.mapper.ClienteMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Collectors;

@DataMongoTest
@ActiveProfiles("test")
class IClienteRepositoryTest {


  @Autowired
  IClienteRepository clienteRepository;

  @Autowired
  MongoTemplate mongoTemplate;

  @BeforeEach
  void setUp() {
    ClienteMapper clienteMapper = new ClienteMapper();
    clienteMapper.setSobrenome("Morais");
    clienteMapper.setDataCriacao(LocalDateTime.now());
    clienteMapper.setNome("Diego");
    clienteMapper.setCpf("999.999.999-99");
    clienteMapper.setEmail("diego@gmail.com");
    clienteMapper.setTelefone("(99) 9 9999-9999");
    clienteMapper.setEndereco("Rua teste");
    mongoTemplate.insert(clienteMapper);
  }

  @AfterEach
  void tearDown() {
    mongoTemplate.dropCollection(ClienteMapper.class);
  }

  @Test
  void devera_buscar_clientes_por_nome_endenreco() {
    PageRequest of = PageRequest.of(0, 10);
    Page<ClienteMapper> cliente = clienteRepository.findByNomeIgnoreCaseContainingAndEnderecoIgnoreCaseContaining("Diego", "Rua teste", of);
    Assertions.assertThat(cliente).isNotNull();
    Assertions.assertThat(cliente.get().collect(Collectors.toList())).isNotEmpty();
    Assertions.assertThat(cliente.getTotalElements()).isEqualTo(1);
    Assertions.assertThat(cliente.get().findFirst().isPresent()).isTrue();
    Assertions.assertThat(cliente.get().findFirst().isEmpty()).isFalse();
    Assertions.assertThat(cliente.get().findFirst().get().getCpf()).isEqualTo("999.999.999-99");
  }

  @Test
  void devera_buscar_cliente_por_email() {
    Optional<ClienteMapper> cliente = clienteRepository.findByEmail("diego@gmail.com");
    Assertions.assertThat(cliente).isNotNull();
    Assertions.assertThat(cliente.isEmpty()).isFalse();
    Assertions.assertThat(cliente.isPresent()).isTrue();
    Assertions.assertThat(cliente.isEmpty()).isFalse();
    Assertions.assertThat(cliente.get().getCpf()).isEqualTo("999.999.999-99");
  }

  @Test
  void devera_buscar_cliente_por_cpf() {
    PageRequest of = PageRequest.of(0, 10);
    Optional<ClienteMapper> cliente = clienteRepository.findByCpf("999.999.999-99");
    Assertions.assertThat(cliente).isNotNull();
    Assertions.assertThat(cliente.isEmpty()).isFalse();
    Assertions.assertThat(cliente.isPresent()).isTrue();
    Assertions.assertThat(cliente.isEmpty()).isFalse();
    Assertions.assertThat(cliente.get().getCpf()).isEqualTo("999.999.999-99");
  }
}