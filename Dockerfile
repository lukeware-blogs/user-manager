FROM maven:3.8.6-amazoncorretto-11 as deploy
WORKDIR /app
COPY src ./src
COPY pom.xml ./pom.xml
RUN mvn clean package

FROM amazoncorretto:11.0.17-al2 as prod
WORKDIR /home
COPY --from=deploy ./app/target/user-manager.jar ./user-manager.jar
USER root:root

ARG SERVER_PORT=9010
ARG DATA_BASE_URI=mongodb://admin:admin123@mongo:27017
ENV SERVER_PORT=${SERVER_PORT}
ENV DATA_BASE_URI=${DATA_BASE_URI}

EXPOSE ${SERVER_PORT}
ENTRYPOINT java -jar -noverify user-manager.jar --server.port=${SERVER_PORT} --spring.data.mongodb.uri=${DATA_BASE_URI}
